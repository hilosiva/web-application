-- データベースの削除
drop database if exists mydb;

-- データベースの作成
create database mydb default character set utf8;

-- データベースの変更
use mydb;

-- テーブルの作成
drop table if exists my_items;
create table my_items (
  id int primary key auto_increment,
  maker_id int,
  item_name text,
  price int,
  keyword text,
  sales int,
  created datetime,
  modified timestamp
) engine=InnoDB default charset=utf8;


drop table if exists makers;
create table makers (
  id int primary key auto_increment,
  name text,
  address text,
  tel text
) engine=InnoDB default charset=utf8;


drop table if exists carts;
create table carts (
  id int primary key auto_increment,
  item_id int,
  count int
) engine=InnoDB default charset=utf8;



insert into my_items (maker_id, item_name, price, keyword, sales, created) values (
  1, 'いちご', 180, '赤い,甘い,ケーキ', 20, now()
), (
  2, 'りんご', 90, '丸い,赤い,パイ', 20, now()
), (
  1, 'バナナ', 120, 'パック,甘い,黄色', 16, now()
), (
  3, 'ブルーベリー', 200, '袋入り,青い,眼精疲労', 8, now()
);



insert into makers (name, address, tel) values (
  '山田さん', '東京都港区', '000-111-2222'
),(
  '斎藤さん', '北海道小樽市', '111-222-3333'
),(
  '川上さん', '神奈川県横浜市', '222-333-4444'
);


insert into carts (item_id, count) values (
  1, 5
),(
  2, 3
),(
  3, 1
), (
  1, 3
), (
  3, 2
), (
  1, 2
);


-- select * from my_items;
-- select * from makers;
-- select * from carts;